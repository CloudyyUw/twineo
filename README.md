# Twineo

Twineo is privacy focused alternative front-end to Twitch inspired by [Invidious](https://github.com/iv-org/invidious) and [Nitter](https://github.com/zedeus/nitter).

Twineo aims to provide:

- No trackers: all the media is requested by the server, no JavaScript fingerprint or IP tracking
- Lightweight and clear: Twitch loads a bunch of unnecessary resources, Twineo on the other hand only loads what you want.
- Open-Source: all the source code for Twineo is fully open under AGPL, so you can inspect, modify or host by your own.

## Screenshots

_A little bit empty for now_

## Instances

| URL                                              | Region | Cloudflare? | Description                                          | Version                                                  |
| ------------------------------------------------ | ------ | ----------- | ---------------------------------------------------- | -------------------------------------------------------- |
| [twineo.exozy.me](https://twineo.exozy.me/)      | 🇺🇸     | No          | Official instance                                    | [twineo.exozy.me/api](https://twineo.exozy.me/api)       |
| [twineo.drgns.space](https://twineo.drgns.space) | 🇺🇸     | No          | [#4](https://codeberg.org/CloudyyUw/twineo/issues/4) | [twineo.drgns.space/api](https://twineo.drgns.space/api) |
| [twineo.ducks.party](https://twineo.ducks.party) | 🇩🇪     | No          | [ducks.party](https://ducks.party) instance | [twineo.ducks.party/api](https://twineo.ducks.party/api) |

---

## Disclaimer

All content on Twineo, and in any of its instances, is hosted by Twitch, so any complaints (such as DMCA or content removal) should be handled by them. Twineo is also not affiliated with Twitch.
